/* vim: set ai et ts=4 sw=4: */
#ifndef __FONTS_H__
#define __FONTS_H__

#include <stdint.h>


//typedef struct {
//    const uint8_t width;
//    uint8_t height;
//    const uint16_t *data;
//} FontDef;

typedef struct {
    const uint8_t width;
    uint8_t height;
    const uint16_t *data;
    const uint8_t *char_width;
} FontDef;




//typedef struct {
//    const uint8_t *data;
//
//    const uint8_t width;
//    uint8_t height;
//} sFONT;



//extern FontDef Font_7x10;
//extern FontDef Font_11x18;
//extern FontDef Font_16x26;x

extern FontDef FontNasalization20;
extern FontDef FontCalibri16;
extern FontDef FontCalibri20;


#endif // __FONTS_H__
