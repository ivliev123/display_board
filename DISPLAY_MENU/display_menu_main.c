/*
 * displa_menu_main.c
 *
 *  Created on: 15 ���. 2024 �.
 *      Author: Dmitry
 */

#include "display_menu_main.h"
#include <stdio.h>
#include "usart.h"
#include "AT24C02N_EEPROM.h"



void Diasplay_init() {
    ST7735_Init();
}

uint8_t flag_Display_Update_Free = 0;


const uint16_t Back_Arrow [] = {0x0000, 0x0000, 0x0000, 0x0000, 0x00F8, 0x0000, 0x03E0, 0x0000, 0x0F80, 0x0000, 0x3E00, 0x0000, 0xFFFF, 0xFE00, 0xFFFF, 0xFF00, 0x3E00, 0x0380, 0x0F80, 0x0180, 0x03E0, 0x0180, 0x00F8, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
const uint16_t Save_Icon  [] = {0x03E0, 0x6700, 0x07E0, 0x6780, 0x07E0, 0x67C0, 0x07E0, 0x67E0, 0x07E0, 0x07F0, 0x07E0, 0x07F0, 0x07FF, 0xFFF0, 0x07FF, 0xFFF0, 0x07FF, 0xFFF0, 0x07E0, 0x07F0, 0x07E0, 0x07F0, 0x07E0, 0x07F0, 0x07E0, 0x07F0, 0x07E0, 0x07F0, 0x07FF, 0xFFF0, 0x07FF, 0xFFF0};



#define FontSize  		16
uint8_t line_len_max 	= (128 - FontSize) / FontSize;

uint8_t Menu_Level = 0;
uint8_t Menu_Level_Last = 0;
uint8_t Menu_Level_MAX = 1;
uint8_t Menu_cursor_Start = 0;
uint8_t Size_Menu_MAX = 0;
//int8_t Menu_Cursor_ENTER = 0;
int8_t Menu_Cursor_ENTER[2] = {0, 0};

uint8_t Line_Var_Change_flag = 0;

#define Size_Menu_Level_0		4
char Menu_Level_0 [Size_Menu_Level_0][16] = {"������", "�������������", "���������", "�������� �����"};

#define Size_Menu_Level_1_1		3
char Menu_Level_1_1 [Size_Menu_Level_1_1][16] = {"�����", "�����", "����" };
#define Size_Menu_Level_1_2		4
char Menu_Level_1_2 [Size_Menu_Level_1_2][16] = {"����� 1", "����� 2", "����� 3", "����� 4"};
#define Size_Menu_Level_1_3		6
char Menu_Level_1_3 [Size_Menu_Level_1_3][16] = {"t �1, �:", "t �2, �:", "T �1, ��:", "T �2, ��:", "L ������, �:", "t �����, �:"};
#define Size_Menu_Level_1_4		2
char Menu_Level_1_4 [Size_Menu_Level_1_4][16] = {"��", "���"};


//float Menu_Level_1_3_VAR [Size_Menu_Level_1_3] = {999.9, 888.8, 6.0, 7.0, 8.0, 9.5};
float Menu_Level_1_3_VAR [Size_Menu_Level_1_3] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};



//#define Main_menu_cursor_MAX	15
//char Main_menu_str [Main_menu_cursor_MAX][16] = {"������", "�������������", "���������", "�������� �����", "Param 1", "Param 2", "Param 3", "Param 4", "Param 5", "Param 6", "Param 7", "Param 8", "Param 9", "Param 10", "Param 11"};
//char cursor_str [] = ">";

int8_t cursor_pos = 0;
int16_t Key_Board_change_flag = 0;

uint8_t line_len = 0;
uint8_t line_pos = 0;
uint8_t first_line_index = 0;

#define Menu_Level_1_3_VAR_EEPROM_Start		32


void init_Menu_Level_1_3_VAR(void){
	uint8_t addr = 0;
	for(int i = 0; i < Size_Menu_Level_1_3; i++){
		addr = Menu_Level_1_3_VAR_EEPROM_Start + i * 4;
		float var = Display_Read_Float_to_EEPROM(addr);
		Menu_Level_1_3_VAR[i] = var;
	}
}

void update_Menu_Level_1_3_VAR(void){
	uint8_t addr = 0;
	for(int i = 0; i < Size_Menu_Level_1_3; i++){
		addr = Menu_Level_1_3_VAR_EEPROM_Start + i * 4;
		Display_Write_Float_to_EEPROM(addr, Menu_Level_1_3_VAR[i]);
	}
}

void Display_Menu_Update(void){
	flag_Display_Update_Free = 0;

	if (Menu_Level == 0){
		Display_Menu_Render(Menu_Level_0, Size_Menu_Level_0, Menu_Level);
		Size_Menu_MAX = Size_Menu_Level_0;
		Display_Cursor_Render( MDB_Info, MDB_Light);

	}

	if (Menu_Level == 1){
		switch(Menu_Cursor_ENTER[0]){
		case 0:
			Display_Menu_Render(Menu_Level_1_1, Size_Menu_Level_1_1, Menu_Level);
			Size_Menu_MAX = Size_Menu_Level_1_1;
			break;
		case 1:
			Display_Menu_Render(Menu_Level_1_2, Size_Menu_Level_1_2, Menu_Level);
			Size_Menu_MAX = Size_Menu_Level_1_2;
			break;
		case 2:
			Display_Menu_Render(Menu_Level_1_3, Size_Menu_Level_1_3, Menu_Level);
//			Display_Var_Render(Menu_Level_1_3_VAR, Size_Menu_Level_1_3, MDB_Dark, MDB_Light);

			for(int i = 0; i < Size_Menu_Level_1_3; i++){
				uint8_t y_pos = Menu_cursor_Start + 16 * i;
				float var = Menu_Level_1_3_VAR[i];
//				float var = Display_Read_Float_to_EEPROM(Menu_Level_1_3_VAR_EEPROM_Start + i * 4);
//				float var = Display_Read_Float_to_EEPROM(0);

				Display_Var_Line_Render(var, y_pos, MDB_Dark, MDB_Light);
			}

			float var = Menu_Level_1_3_VAR[cursor_pos];
			Display_Var_Line_Change(var, cursor_pos, MDB_Dark, MDB_Light, MDB_Info);

			Size_Menu_MAX = Size_Menu_Level_1_3;
			break;
		case 3:
			Display_Menu_Render(Menu_Level_1_4, Size_Menu_Level_1_4, Menu_Level);
			Size_Menu_MAX = Size_Menu_Level_1_4;
			break;
		default:
			break;
		}
		Display_Cursor_Render( MDB_Info, MDB_Light);
	}


}


BUTTON BTN_UP;
BUTTON BTN_DOWN;
BUTTON BTN_ENTER;
BUTTON BTN_LEFT;
BUTTON BTN_RIGHT;



uint8_t ButtonClick(BUTTON *BTN_, GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin){
	uint8_t ClickStatus = 0;

	BTN_ -> Status_now = (uint16_t)HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);
	if(BTN_ -> Status_now == 0 && BTN_ -> Status_last == 1){
		ClickStatus = 1;
	}
	else{
		ClickStatus = 0;
	}

	BTN_ -> Status_last = BTN_ -> Status_now;
	return ClickStatus;
}

uint8_t Write_EEPROM_flag = 0;

void Key_Board_CallBack(void){

	uint8_t BTN_UP_ClickStatus =  ButtonClick(&BTN_UP, BTN_UP_GPIO_Port, BTN_UP_Pin);
	uint8_t BTN_DOWN_ClickStatus =  ButtonClick(&BTN_DOWN, BTN_DOWN_GPIO_Port, BTN_DOWN_Pin);
	uint8_t BTN_LEFT_ClickStatus =  ButtonClick(&BTN_LEFT, BTN_LEFT_GPIO_Port, BTN_LEFT_Pin);
	uint8_t BTN_RIGHT_ClickStatus =  ButtonClick(&BTN_RIGHT, BTN_RIGHT_GPIO_Port, BTN_RIGHT_Pin);
	uint8_t BTN_ENTER_ClickStatus =  ButtonClick(&BTN_ENTER, BTN_ENTER_GPIO_Port, BTN_ENTER_Pin);



	Menu_Surf_Callback(BTN_UP_ClickStatus, BTN_DOWN_ClickStatus, BTN_LEFT_ClickStatus, BTN_RIGHT_ClickStatus, BTN_ENTER_ClickStatus);

	if(Menu_Cursor_ENTER[0] == 2){
		Menu_Chenge_Var_Callback(Menu_Level_1_3_VAR, cursor_pos, BTN_UP_ClickStatus, BTN_DOWN_ClickStatus, BTN_LEFT_ClickStatus, BTN_RIGHT_ClickStatus, BTN_ENTER_ClickStatus);
		Menu_Surf_Chenge_Callback(BTN_UP_ClickStatus, BTN_DOWN_ClickStatus, BTN_LEFT_ClickStatus,  BTN_RIGHT_ClickStatus,  BTN_ENTER_ClickStatus);
	}
	// if elso cursor ENTER, then use also array and this construction.


	if(BTN_ENTER_ClickStatus){
		if(cursor_pos == -1 && Line_Var_Change_flag == 0){Menu_Level--;}
		else if(cursor_pos == -1 && Line_Var_Change_flag == 1){
//			Save

			Line_Var_Change_flag = 0;
		}
		else{
			Menu_Cursor_ENTER[Menu_Level] = cursor_pos;
			Menu_Level++;
			if (Menu_Level > Menu_Level_MAX){Menu_Level = Menu_Level_MAX;}
//			else{Menu_Cursor_ENTER = cursor_pos;}
		}
		cursor_pos = 0;
		Key_Board_change_flag = 1;

	}



	if(Key_Board_change_flag == 1){
		Display_Menu_Update();
		Key_Board_change_flag = 0;

		char test [] = "  ";
		test [0] = cursor_pos + '0';
		ST7735_WriteString(50, 100, test, 	FontCalibri16, MDB_Success, MDB_Light);
//		flag_Display_Update_Free = 1;
	}
}


void Menu_Chenge_Var_Callback(float var_array[], uint8_t id, uint8_t BTN_UP_ClickStatus, uint8_t BTN_DOWN_ClickStatus, uint8_t BTN_LEFT_ClickStatus, uint8_t BTN_RIGHT_ClickStatus, uint8_t BTN_ENTER_ClickStatus){
	if (Line_Var_Change_flag){
		float chenve_var = var_array[id];

		uint8_t Menu_Chenge_Var_update_flag = 0;
		if(BTN_UP_ClickStatus){
			chenve_var = chenve_var + 0.1;
			Menu_Chenge_Var_update_flag = 1;
		}

		if(BTN_DOWN_ClickStatus){
			chenve_var = chenve_var - 0.1;
			Menu_Chenge_Var_update_flag = 1;
		}
		if(chenve_var >= 999.9){chenve_var = 999.9;}
		if(chenve_var <= 0.0){chenve_var = 0.0;}

		if(Menu_Chenge_Var_update_flag){
			var_array[id] = chenve_var;
			Write_EEPROM_flag = 1;
			Display_Write_Float_to_EEPROM(Menu_Level_1_3_VAR_EEPROM_Start + id * 4, chenve_var);

			Display_Var_Line_Change(chenve_var, cursor_pos, MDB_Dark, MDB_Light, MDB_Info);
			Menu_Chenge_Var_update_flag = 0;
		}
	}
}

//void Menu_Chenge_Var_Write_EEPROM(float var_array[], uint8_t id){
//	Display_Write_Float_to_EEPROM(Menu_Level_1_3_VAR_EEPROM_Start + id * 4, var_array[id]);
//}




void Menu_Surf_Callback(uint8_t BTN_UP_ClickStatus, uint8_t BTN_DOWN_ClickStatus, uint8_t BTN_LEFT_ClickStatus, uint8_t BTN_RIGHT_ClickStatus, uint8_t BTN_ENTER_ClickStatus){
	if (!Line_Var_Change_flag){
		if(BTN_UP_ClickStatus){
			cursor_pos = cursor_pos -1;

			if(Menu_Level > 0){
				if(cursor_pos <= -1){cursor_pos = -1;}
			}
			else{
				if(cursor_pos <= 0){cursor_pos = 0;}
			}

			Key_Board_change_flag = 1;
		}
		if(BTN_DOWN_ClickStatus){
			cursor_pos = cursor_pos + 1;
			if(cursor_pos >= Size_Menu_MAX - 1){
				cursor_pos =  Size_Menu_MAX - 1;
			}
			Key_Board_change_flag = 1;
		}
	}
}


void Menu_Surf_Chenge_Callback(uint8_t BTN_UP_ClickStatus, uint8_t BTN_DOWN_ClickStatus, uint8_t BTN_LEFT_ClickStatus, uint8_t BTN_RIGHT_ClickStatus, uint8_t BTN_ENTER_ClickStatus){
	if(BTN_LEFT_ClickStatus){
		Line_Var_Change_flag = 0;
		Key_Board_change_flag = 1;
	}

	if(BTN_RIGHT_ClickStatus){
		if(Write_EEPROM_flag){
//			update_Menu_Level_1_3_VAR();
//			Menu_Chenge_Var_Write_EEPROM(Menu_Level_1_3_VAR, cursor_pos);
			Write_EEPROM_flag = 0;
		}
		Line_Var_Change_flag = 1;
		Key_Board_change_flag = 1;
	}
}



void Display_Menu_Render(char Menu_Level_[][16], uint8_t Size_Menu_Level_, uint8_t Level_){
	  ST7735_FillScreen(MDB_Light);

	  if(Menu_Level == 0){
		  Menu_cursor_Start = 0;
	  }
	  else{
		  Menu_cursor_Start = FontSize;
	  }
	  uint8_t cursor_pos_local = 0;

	  if(cursor_pos == -1){
		  cursor_pos_local = 0;
	  }
	  else{
		  cursor_pos_local = cursor_pos;
	  }

	  if (cursor_pos_local + 1 > (Size_Menu_Level_ / line_len_max) * line_len_max){
		  line_len = Size_Menu_Level_ % line_len_max;
	  }
	  else{
		  line_len = line_len_max;
	  }
	  first_line_index = (cursor_pos_local / line_len_max) * line_len_max;

	  for(int i = 0; i < line_len; i++){
		  ST7735_WriteString(16, Menu_cursor_Start + 16 * i, Menu_Level_[first_line_index + i], FontCalibri16, MDB_Dark, MDB_Light);
	  }


}


void Display_Var_Render(float *var_array, uint8_t size_var_array, uint16_t color, uint16_t bgcolor){

	for(int i = 0; i < size_var_array; i++){
		float var = *var_array;
		char str[4];
		sprintf(str, "%d.%01d", (int)var, (int)((var - (int)var)*10.));
		ST7735_WriteString(110, Menu_cursor_Start + 16 * i, str, FontCalibri16, color, bgcolor);

		var_array++;
	}
}

char str2[10];


void Display_Var_Line_Change(float var, int8_t line, uint16_t color, uint16_t bgcolor, uint16_t bgcolor_chenge){
	if(cursor_pos != -1){
		if (Line_Var_Change_flag){
			char str[4]= {};
			sprintf(str, "%d.%01d", (int)var, (int)((var - (int)var)*10.));
			ST7735_WriteString(110, Menu_cursor_Start + 16 * line, str, FontCalibri16, color, bgcolor_chenge);
		}
		else{
		}
	}
}


void Display_Var_Line_Render(float var, uint8_t y_pos, uint16_t color, uint16_t bgcolor){
		char str[4] = {};
		sprintf(str, "%d.%01d", (int)var, (int)((var - (int)var)*10.));
		ST7735_WriteString(110, y_pos, str, FontCalibri16, color, bgcolor);
}

void Display_Cursor_Render(uint16_t color, uint16_t bgcolor){
	  if(Menu_Level >= 1){
		  if(cursor_pos == -1){
			  if(Line_Var_Change_flag == 0){
				  ST7735_WriteSymbol(0, 0, Back_Arrow, bgcolor, color);
			  }
			  else{
				  ST7735_WriteSymbol(0, 0, Back_Arrow, color, bgcolor);
			  }
		  }
		  else{
			  ST7735_WriteSymbol(0, 0, Back_Arrow, color, bgcolor);
		  }
	  }
	  if(Menu_Level == 1 && Menu_Cursor_ENTER[0] == 2){
		  if(cursor_pos == -1){
			  if(Line_Var_Change_flag == 1){
				  ST7735_WriteSymbol(128, 0, Save_Icon, bgcolor, color);
			  }
			  else{
				  ST7735_WriteSymbol(128, 0, Save_Icon, color, bgcolor);
			  }
		  }
		  else{
			  ST7735_WriteSymbol(128, 0, Save_Icon, color, bgcolor);
		  }
	  }

	  if(cursor_pos >=0){
		  line_pos = cursor_pos % line_len_max + Menu_cursor_Start / FontSize;
		  uint8_t cursor_pos_point = line_pos * FontSize;
//		  ST7735_WriteString(0, cursor_pos_point, cursor_str, FontCalibri16, color, bgcolor);
		  ST7735_Select();
		  ST7735_WriteCharCUSTOM(0, cursor_pos_point, '>',  FontCalibri16, color, bgcolor);

		  rect(0, cursor_pos_point, 160-1, 16, color);
	  }
}


void rect(int X, int Y, int W, int H,  uint16_t color) {

    for(int x = 0; x < W; x++) {
        ST7735_DrawPixel(x + X, Y, color);
        ST7735_DrawPixel(x + X, Y+H, color);
    }
    for(int y = 0; y < H; y++) {
        ST7735_DrawPixel(X, y + Y, color);
        ST7735_DrawPixel(X+W, y + Y, color);
    }
}


