/*
 * AT24C02N_EEPROM.c
 *
 *  Created on: 19 ���. 2024 �.
 *      Author: Dmitry
 */

#include "AT24C02N_EEPROM.h"
#include "I2C_software.h"
//#include <stdlib.h>
//#include <string.h>
#include "delay.h"
#include "display_menu_main.h"


uint8_t ADDR_new = 0;


void AT24C02N_Write_Byte(uint8_t data_addr, uint8_t data){
//  Wire.beginTransmission(ADDR);
//  Wire.write(data_addr);
//  Wire.write(data);
//  Wire.endTransmission();

	ADDR_new = (ADDR << 1) + 0;

	i2c_start_cond ();
	i2c_send_byte (ADDR_new);
	i2c_send_byte (data_addr);
	i2c_send_byte (data);
	i2c_stop_cond();
}

uint8_t AT24C02N_Read_Byte(uint8_t data_addr){
	uint8_t data = 0;

//  Wire.beginTransmission(ADDR);
//  Wire.write(data_addr);
//  Wire.endTransmission();
//  Wire.requestFrom(ADDR, 1); //retrieve 1 returned byte
//  delay(1);
//  if(Wire.available()){
//    data = Wire.read();
//  }

	ADDR_new = (ADDR << 1) + 0;

	i2c_start_cond ();
	i2c_send_byte (ADDR_new);
	i2c_send_byte (data_addr);
	i2c_restart_cond ();

	ADDR_new = (ADDR << 1) + 1;

	i2c_send_byte (ADDR_new);
	data = i2c_get_byte(0);
	i2c_stop_cond();

	return data;
}


void Display_Write_Float_to_EEPROM(uint8_t data_addr, float var){
	uint8_t byte = 0;
	for(int i=0; i<4; i++){
	  byte = *((uint8_t *)&var + i);
	  AT24C02N_Write_Byte(data_addr + i, byte);
	}
}

uint8_t byte_array_f[4]= {0, 0, 0, 0};

float Display_Read_Float_to_EEPROM(uint8_t data_addr){
	float var_f = 0.0;
	for(int i=0; i<4; i++){
//		byte_array_f[i] = AT24C02N_Read_Byte(data_addr + i);
		uint8_t byte_test = AT24C02N_Read_Byte(data_addr + i);
		byte_array_f[i] = byte_test;

	}
	memcpy(&var_f, &byte_array_f, sizeof(var_f));
	return var_f;
}
