/*
 * AT24C02N_EEPROM.h
 *
 *  Created on: 19 ���. 2024 �.
 *      Author: Dmitry
 */

#ifndef AT24C02N_EEPROM_H_
#define AT24C02N_EEPROM_H_

#include "main.h"

#define ADDR_Ax 0b000 //A2, A1, A0
#define ADDR (0b1010 << 3) + ADDR_Ax

void AT24C02N_Write_Byte(uint8_t data_addr, uint8_t data);
uint8_t AT24C02N_Read_Byte(uint8_t data_addr);

void Display_Write_Float_to_EEPROM(uint8_t data_addr, float var);
float Display_Read_Float_to_EEPROM(uint8_t data_addr);

#endif /* AT24C02N_EEPROM_H_ */
