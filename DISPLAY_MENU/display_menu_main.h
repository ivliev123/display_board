/*
 * displa_menu_main.h
 *
 *  Created on: 15 ���. 2024 �.
 *      Author: Dmitry
 */

#include <string.h>
#include "st7735.h"
#include "fonts.h"
#include "testimg.h"




#ifndef DISPLAY_MENU_MAIN_H_
#define DISPLAY_MENU_MAIN_H_

#define MDB_Primary 	ST7735_COLOR565(59, 113, 202)
#define MDB_Secondary	ST7735_COLOR565(159, 166, 178)
#define MDB_Success		ST7735_COLOR565(20, 164, 77)
#define MDB_Danger		ST7735_COLOR565(220, 76, 100)
#define MDB_Warning		ST7735_COLOR565(228, 161, 27)
#define MDB_Info 		ST7735_COLOR565(84, 180, 211)
#define MDB_Light 		ST7735_COLOR565(251, 251, 251)
#define MDB_Dark 		ST7735_COLOR565(51, 45, 45)


struct button{
    uint8_t Status_now;
    uint8_t Status_last;
};
typedef struct button BUTTON;




void Diasplay_init();

void Display_Menu_Render(char Menu_Level_[][16], uint8_t Size_Menu_Level_, uint8_t Level_);
void Display_Var_Render(float var_array[], uint8_t size_var_array, uint16_t color, uint16_t bgcolor);
void Display_Cursor_Render(uint16_t color, uint16_t bgcolor);

void Display_Menu_Update(void);
void rect(int X, int Y, int W, int H,  uint16_t color);
uint8_t ButtonClick(BUTTON *BTN_, GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
void Key_Board_CallBack(void);

void Menu_Surf_Callback(uint8_t BTN_UP_ClickStatus, uint8_t BTN_DOWN_ClickStatus, uint8_t BTN_LEFT_ClickStatus, uint8_t BTN_RIGHT_ClickStatus, uint8_t BTN_ENTER_ClickStatus);
//void Display_Var_Line_Change(float var_array[], uint8_t line, uint16_t color, uint16_t bgcolor, uint16_t bgcolor_chenge);
void Display_Var_Line_Change(float var, int8_t line, uint16_t color, uint16_t bgcolor, uint16_t bgcolor_chenge);
//void Display_Var_Line_Render(float var_array[], uint8_t line, uint8_t size_var_array, uint16_t color, uint16_t bgcolor);
void Display_Var_Line_Render(float var, uint8_t y_pos, uint16_t color, uint16_t bgcolor);
void Menu_Chenge_Var_Callback(float var_array[], uint8_t id, uint8_t BTN_UP_ClickStatus, uint8_t BTN_DOWN_ClickStatus, uint8_t BTN_LEFT_ClickStatus, uint8_t BTN_RIGHT_ClickStatus, uint8_t BTN_ENTER_ClickStatus);
void Menu_Surf_Chenge_Callback(uint8_t BTN_UP_ClickStatus, uint8_t BTN_DOWN_ClickStatus, uint8_t BTN_LEFT_ClickStatus, uint8_t BTN_RIGHT_ClickStatus, uint8_t BTN_ENTER_ClickStatus);


void init_Menu_Level_1_3_VAR(void);


#endif /* DISPLAY_MENU_MAIN_H_ */
