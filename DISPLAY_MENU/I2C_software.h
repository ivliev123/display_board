/*
 * I2C_software.h
 *
 *  Created on: 19 ���. 2024 �.
 *      Author: Dmitry
 */

#ifndef I2C_SOFTWARE_H_
#define I2C_SOFTWARE_H_

#include "stm32f1xx_hal.h"

//  CUBE MX  I2C    (  main.c      GPIO)

#define I2C_SDA_GPIO_Port	GPIOB
#define I2C_SCL_GPIO_Port	GPIOB
#define I2C_SDA_GPIO_Pin	GPIO_PIN_9
#define I2C_SCL_GPIO_Pin	GPIO_PIN_8

//---   -----------------------------------------------------
#define SCL_I HAL_GPIO_ReadPin(I2C_SCL_GPIO_Port, I2C_SCL_GPIO_Pin);
#define SDA_I HAL_GPIO_ReadPin(I2C_SDA_GPIO_Port, I2C_SDA_GPIO_Pin);
#define SCL_O HAL_GPIO_WritePin(I2C_SCL_GPIO_Port, I2C_SCL_GPIO_Pin, GPIO_PIN_RESET);
#define SDA_O HAL_GPIO_WritePin(I2C_SDA_GPIO_Port, I2C_SDA_GPIO_Pin, GPIO_PIN_RESET);
//--------------------------------------------------------------------------------

void SCL_in (void);
void SCL_out (void);
void SDA_in (void);
void SDA_out (void);

// void Delay_us (uint32_t  us);

void i2c_init (void);               //
void i2c_start_cond (void);        //
void i2c_restart_cond (void);      //
void i2c_stop_cond (void) ;       //
uint8_t i2c_send_byte (uint8_t data) ;      //  (.   ) ( 0 - , 1 - NACK)
uint8_t i2c_get_byte (uint8_t last_byte) ;  //  (      = 1,      0)(  )
//--------------------------------------------------------------------------------
//
//=========================================================================================
//    uint16_t    (FRAM FM24CL64)    24LC ,
//	  ,     ,   adr++
//=========================================================================================
//
//void FRAM_W_INT(uint16_t adr, uint16_t dat){
//i2c_start_cond ();
//i2c_send_byte (0xA2); //  +    ()
//i2c_send_byte    ((adr & 0xFF00) >> 8);
//i2c_send_byte    (adr & 0x00FF);
//i2c_send_byte    ((dat & 0xFF00) >> 8);
//i2c_send_byte    (dat & 0x00FF);
//i2c_stop_cond();
//}

//=========================================================================================
//    uint16_t    (FRAM FM24CL64)    24LC ,
//	  ,     ,   adr++
//=========================================================================================
//uint16_t FRAM_R_INT(uint16_t adr){
//uint16_t dat;
//i2c_start_cond ();
//i2c_send_byte (0xA2);
//i2c_send_byte    ((adr & 0xFF00) >> 8);
//i2c_send_byte    (adr & 0x00FF);
//i2c_restart_cond ();
//i2c_send_byte (0xA3);
//dat =  i2c_get_byte(0);
//dat <<= 8;
//dat |= i2c_get_byte(1);
//i2c_stop_cond();
//return dat;
//}


#endif /* I2C_SOFTWARE_H_ */
