/*
 * delay.c
 *
 *  Created on: 29 ���. 2024 �.
 *      Author: Ivliev
 */

#include "delay.h"

extern TIM_HandleTypeDef htim3;


void TimerDelay_Init(void)
{
//	uint16_t gu32_ticks = (HAL_RCC_GetHCLKFreq() / 1000000);
//	uint16_t gu32_ticks = 108;
//	uint16_t gu32_ticks = 216;
	uint16_t gu32_ticks = 72-1;
	htim3.Instance = TIM3;
	htim3.Init.Prescaler = gu32_ticks-1;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 10000;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
	{
	Error_Handler();
	}

    HAL_TIM_Base_Start(&htim3);

}


void Delay_us(uint16_t au16_us)
{
	htim3.Instance->CNT = 0;
    while (htim3.Instance->CNT < au16_us);
}


void Delay_ms(uint16_t au16_ms)
{
    while(au16_ms > 0)
    {
    	htim3.Instance->CNT = 0;
		au16_ms--;
		while (htim3.Instance->CNT < 1000);
    }
}
